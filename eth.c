#include <string.h>
#include "type.h"
#include "Net_Config.h"
#include <stdio.h>
#include "eth.h"
#include "Main.h"
#include "glcd.h"

#define MY_IP localm[NETIF_ETH].IpAdr
#define MY_MASK localm[NETIF_ETH].NetMask
#define MY_GW localm[NETIF_ETH].DefGW
#define MY_DNS1 localm[NETIF_ETH].PriDNS
#define MY_DNS2 localm[NETIF_ETH].SecDNS
#define REC_PORT 1337
#define SEND_PORT 6969

U8 output_buf[MAX_NLOG_BUF];

extern LOCALM localm[]; /* Local Machine Settings      */
U8 tcp_soc[2] = {NULL, NULL};
U8 soc_state;
BOOL wait_ack;

struct tcp_client {
  U8 ip[4];
  U32 packet_count;
};

#include "Delay.h"

BOOL send_data(void) {

  main_TcpNet();

  static const U8 rem_IP[4] = {10, 0, 0, 50};
  static int bcount;
  U32 max;
  U8 *sendbuf;
  U16 i = 0;

  switch (soc_state) {
  case 0:
    tcp_connect(tcp_soc[1], (U8 *)rem_IP, SEND_PORT, 0);
    bcount = 0;
    wait_ack = __FALSE;
    soc_state = 1;
    return FALSE;

  case 2:
    if (wait_ack == __TRUE) {
      return FALSE;
    }
    max = tcp_max_dsize(tcp_soc[1]);
    sendbuf = tcp_get_buf(max);

    for (; i < max; i += 2) {
      sendbuf[i] = bcount >> 8;
      sendbuf[i + 1] = bcount & 0xFF;
      if (bcount >= 32768) {
        soc_state = 3;
        break;
      }
    }

    strcpy((S8 *)sendbuf, (S8 *)output_buf);
    memset(output_buf, 0, sizeof(output_buf));
    tcp_send(tcp_soc[1], sendbuf, i);

    wait_ack = __TRUE;

    return TRUE;
  case 3:
    if (wait_ack == __TRUE) {
      return FALSE;
    }
    tcp_close(tcp_soc[1]);
    soc_state = 4;
    return FALSE;
  }
  return 3;
}

U32 get_spc_location(U8 *arr, U32 arr_sz) {

  U32 ret = 0;
  while (ret < arr_sz && arr[ret] != ' ')
    ret++;
  return ret;
}

#include <stdlib.h>
#include "dynmem.h"

void c_aslloc_rem(U8 *packet) {
  S32 size = strtoul((S8 *)packet, NULL, 10);
  U8 *cur_p = aslloc(size);
  N_LOG("ALLOCATED [%d] bytes at [ 0x%p ]", size, cur_p);
}

void c_asfree_rem(U8 *packet) {
  S32 addr = strtoul((S8 *)packet, NULL, 0);
  asfree((void *)addr);
  N_LOG("FREED at [ 0x%p ]", (void *)addr);
}

void c_read_byte(U8 *packet) {

  S32 hex_input = strtoul((S8 *)packet, NULL, 0);

  if (hex_input > 0xD0000000 || hex_input < 0xA0000000) {
    N_LOG("[0x%s]->not valid addr]", packet);
    K_LOG("err:readb()->%s", packet);
    return;
  }

  N_LOG("addr->[0x%p] val=[0x%x]", (S8 *)hex_input, *(S8 *)hex_input);
  K_LOG("c->readb()=%s", packet);
}

void c_read_sector(U8 *packet, U32 packet_sz) {

  U32 last_arg_loc = get_spc_location(packet, packet_sz) + 1;

  packet[last_arg_loc - 1] = '\0';

  S32 start_mem = strtoul((S8 *)packet, NULL, 0);
  S32 end_mem = strtoul((S8 *)packet + last_arg_loc, NULL, 0);

  // N_LOG("val = [0x%x] -> &0x%p", input_val, (U8 *)input_addr);
  U16 j = 0;
  for (U8 *i = (U8 *)start_mem; i <= (U8 *)end_mem; i++, j++) {
    if (i < (U8 *)0xD0000000 && i >= (U8 *)0xA0000000) {
      // N_LOG("[%d]addr->[0x%p] val=[0x%x]", j, i, *i);
      K_LOG("[0x%p][0x%x]", i, *i);
    }
  }
}

#include "glcd.h"

void c_clear_lcd(void) {
  clrlcd();
  N_LOG("PHYSICAL LCD CLEARED!");
}

void c_print_scr(U8 *packet, U32 packet_sz) {

  U32 last_arg_loc = get_spc_location(packet, packet_sz) + 1;
  packet[last_arg_loc - 2] = '\0';

  S32 arg1 = strtoul((S8 *)packet, NULL, 0);
  S32 arg2 = strtoul((S8 *)packet + last_arg_loc, NULL, 0);

  N_LOG("at X[%d]Y[%d]val = [%x]", arg1, arg2, virtual_scr[arg1][arg2]);
}

void c_write_byte(U8 *packet, U32 packet_sz) {

  U32 last_arg_loc = get_spc_location(packet, packet_sz) + 1;

  if (last_arg_loc - 1 == packet_sz || last_arg_loc == packet_sz) {
    N_LOG("WRITE_BYTE:\narg[1] = addr of memory\narg[2] = hex val to write\n");
    K_LOG("err:writeb->%s", packet);
  }

  packet[last_arg_loc - 1] = '\0';

  K_LOG("1 arg =%s", packet);
  K_LOG("2 arg =%s", packet + last_arg_loc);

  S32 input_addr = strtoul((S8 *)packet, NULL, 0);
  S32 input_val = strtoul((S8 *)packet + last_arg_loc, NULL, 0);

  K_LOG("[0x%x] at 0x%p", input_val, (U8 *)input_addr);

  if (input_addr > 0xD0000000 || input_addr < 0xA0000000) {
    N_LOG("[0x%x]->not valid addr]", input_addr);
    K_LOG("err:readb()->%x", input_addr);
    return;
  }

  *(U8 *)input_addr = input_val;

  N_LOG("0x%p -> 0x%x", (U8 *)input_addr, input_val);
}

void c_echo(U8 *packet) {
  strcpy((S8 *)output_buf, (S8 *)packet);
  K_LOG("c->echo()=%s", packet);
  send_data();
  // while (send_data() == FALSE) {};
}

void c_free_all(void) {

  // NEEDS THIS BECUASE OF THE TCP DELAY REPATING INSTRUCTIONS
  U16 hacky_fix_tcp_delay = total_alloc_cnk_sum();
  N_LOG("FREEING %d bytes", hacky_fix_tcp_delay);
  U32 total_free = asfree_all();
  K_LOG("FREED %d bytes", total_free);
  N_LOG("DONE! %d BYTES", total_free);
}

void c_free_last(void) {

  void *last_freed = free_last_alloc();

  // K_LOG("l-freed=%p",last_freed);
  N_LOG("FREED LAST ALLOC [ 0x%p ]", last_freed);
}

void c_pong(void) {
  N_LOG("ping from %d.%d.%d.%d:%d", MY_IP[0], MY_IP[1], MY_IP[2], MY_IP[3],
        SEND_PORT);
  K_LOG("c->ping=%d.%d.%d.%d", MY_IP[0], MY_IP[1], MY_IP[2], MY_IP[3]);
}

#include "cmd_list.h"

void c_help(void) {

  output_buf[0] = '\n';
  S8 *p_write_buf = (S8 *)output_buf + 1;

  for (U16 j = 0; j < *(&cur_cmd + 1) - cur_cmd; j++) {
    sprintf(p_write_buf, "\n%d: %s", j + 1, cur_cmd[j]);
    p_write_buf += strlen(cur_cmd[j]) + 4;
  }
  while (send_data() == FALSE)
    ;
}

void cmd_exec(U8 *packet, U16 packet_sz, U16 cmd_num) {

  switch (cmd_num) {

  case ECHO:
    c_echo(packet);
    break;

  case READ_BYTE:
    c_read_byte(packet);
    break;

  case WRITE_BYTE:
    c_write_byte(packet, packet_sz);
    break;

  case PING:
    c_pong();
    break;

  case PRINT_SCR:
    c_print_scr(packet, packet_sz);
    break;

  case READ_SECTOR:
    c_read_sector(packet, packet_sz);
    break;

  case ASLLOC_REM:
    c_aslloc_rem(packet);
    break;

  case ASFREE_REM:
    c_asfree_rem(packet);
    break;

  case FREE_ALL:
    c_free_all();
    break;

  case FREE_LAST:
    c_free_last();
    break;

  case HELP:
    c_help();
    break;

  case CLEAR_LCD:
    c_clear_lcd();
    break;

  default:
    K_LOG("exec:err = %d", cmd_num);
    N_LOG("exec:err = %d", cmd_num);
    break;
  }
}

void cmd_parse(U8 *packet, U16 packet_size) {

  if (packet_size >= MAX_NLOG_BUF) {
    K_LOG("max[%d] < packet_size[%d]", MAX_NLOG_BUF, packet_size);
    N_LOG("max[%d] < packet_size[%d]", MAX_NLOG_BUF, packet_size);
    return;
  }

  U16 spc_loc = get_spc_location(packet, packet_size);

  packet[packet_size - 1] = '\0';

  if (spc_loc) {
    for (U16 j = 0; j < *(&cur_cmd + 1) - cur_cmd; j++) {
      // N_LOG("%s == %s",cur_cmd[j],packet);
      // N_LOG("%s %d == packet_size %d",cur_cmd[j],strlen(cur_cmd[j]),spc_loc);
      if (strncmp(cur_cmd[j], (S8 *)packet, spc_loc) == 0 &&
          strlen(cur_cmd[j]) == spc_loc) {
        cmd_exec(packet + spc_loc + 1, packet_size - spc_loc + 1, j);
        return;
      }
    }
  }

  static U32 n_line = 0;

  K_LOG("%s", packet);
  N_LOG("[%d] unkown Asafos cmd: [%s]", n_line++, packet);
}

// tcp[0]
U16 tcp_send_callback(U8 soc, U8 event, U8 *ptr, U16 par) {
  /* This function is called on TCP event */
  switch (event) {
  case TCP_EVT_CONREQ:
    /* Remote host is trying to connect to our TCP socket. */
    /* 'ptr' points to Remote IP, 'par' holds the remote port. */
    K_LOG("tcp:snd:con->%d.%d.%d.%d", ptr[0], ptr[1], ptr[2], ptr[3]);
    /* Return 1 to accept connection, or 0 to reject connection */
    return (1);
  case TCP_EVT_ABORT:
    /* Connection was aborted */
    // soc_state = 0;
    tcp_abort(tcp_soc[1]);
    K_LOG("tcp:snd:ABORT");
    break;
  case TCP_EVT_CONNECT:
    /* Socket is connected to remote peer. */
    soc_state = 2;
    K_LOG("tcp:snd:CONNECT");
    break;
  case TCP_EVT_CLOSE:
    /* Connection has been closed */
    // soc_state = 0;
    // tcp_close(tcp_soc[0]);
    K_LOG("tcp:snd:CLOSE");
    // snprintf((S8 *)output_buf, par, "tcp:snd:con->%d.%d.%d.%d", ptr[0],
    // ptr[1],ptr[2], ptr[3]);
    break;
  case TCP_EVT_ACK:
    /* Our sent data has been acknowledged by remote peer */
    wait_ack = __FALSE;
    // K_LOG("tcp:snd:ACK");
    break;
  case TCP_EVT_DATA:
    /* TCP data frame has been received, 'ptr' points to data */
    /* Data length is 'par' bytes */

    // K_LOG("tcp:snd:err:evt->data");
    cmd_parse(ptr, par);
    break;
  }
  return (0);
}

void init_send_client(void) {
  tcp_soc[1] = tcp_get_socket(TCP_TYPE_CLIENT, 0, 120, tcp_send_callback);
  soc_state = 0;
  while (send_data() == FALSE)
    ;
}

U16 tcp_recive_callback(U8 soc, U8 event, U8 *ptr, U16 par) {
  /* This function is called on TCP event */
  switch (event) {
  case TCP_EVT_CONREQ:
    /* Remote host is trying to connect to our TCP socket. */
    /* 'ptr' points to Remote IP, 'par' holds the remote port. */
    K_LOG("tcp:rec:con->%d.%d.%d.%d", ptr[0], ptr[1], ptr[2], ptr[3]);

    /* Return 1 to accept connection, or 0 to reject connection */
    return (1);
  case TCP_EVT_ABORT:
    /* Connection was aborted */
    K_LOG("tcp:rec:ABORT");
    break;
  case TCP_EVT_CONNECT:
    /* Socket is connected to remote peer. */
    K_LOG("tcp:rec:CONNECT");
    init_send_client();
    break;
  case TCP_EVT_CLOSE:
    /* Connection has been closed */
    // soc_state = 0;
    // tcp_close(tcp_soc[1]);
    K_LOG("tcp:rec:CLOSE");
    break;
  case TCP_EVT_ACK:
    /* Our sent data has been acknowledged by remote peer */
    K_LOG("tcp:rec:ACK");
    // wait_ack = __FALSE;// expermental
    break;
  case TCP_EVT_DATA:
    /* TCP data frame has been received, 'ptr' points to data */
    /* Data length is 'par' bytes */
    cmd_parse(ptr, par);
    break;
  }
  return (0);
}
