
extern U32 init_timer(U8 timer_num, U32 timerInterval);
extern void enable_timer(U8 timer_num);
extern void disable_timer(U8 timer_num);
extern void reset_timer(U8 timer_num);

/*****************************************************************************
**                            End Of File
******************************************************************************/

// extern void init_timer (void);
extern __irq void T0_IRQHandler(void);
extern U32 cntsec;
extern U16 cnt1ms;
