#include <stdio.h>
#include "glcd.h"
#include "type.h"

// debug printout
#include "k_log_tog.h"

// addr specfied in the LPC23xx def for dynamic mem
#define DYNAMIC_MEM0_BASE 0xA0000000

// diffrent buckets for allocation size fragmentation optimization
#define DYNAMIC_MEM1_BASE 0xB0000000
#define DYNAMIC_MEM2_BASE 0xC0000000
#define DYNAMIC_MEM3_BASE 0xD0000000

#define PAGE_ALIGNMENT 8

#include "dyn_ram.h"

typedef S64 PAGE_SIZE;

static void *chunk_start = &inital_memory_assertion;

// number of allocated chunks
static U32 heap_size, total_allocated_mm = 0;
static void *heap_head = NULL;

// meta chunk allocaion information [CURRENT SIZE = 0x9]
struct chunk {

  // number of allocated bytes in the chunks
  U32 size;

  BYTE flags;
// bit 1 is the current chunk used or freed
#define ALLOCATED_FLG 0
// bit 2 is used for if the chunk is the first chunk allocated
#define FIRST_CNK_FLG 1
// bit 3 for if chunk alloc is in non volatile memory.
#define NON_VOLATILE_FLG 2

  // location for the next requested allocation chunk
  struct chunk *next_chunk;

}; //__attribute__ ((aligned (PAGE_ALIGNMENT)));

struct chunk *combine_chunks(struct chunk *first_chunk) {

  if (((first_chunk->next_chunk->flags >> ALLOCATED_FLG) & 1U) == TRUE ||
      first_chunk->next_chunk == heap_head) {
    return first_chunk;
  }

  U32 sum = 0;
  struct chunk *head = first_chunk;
  struct chunk *tmp = first_chunk->next_chunk;

  while (((tmp->flags >> ALLOCATED_FLG) & 1U) == FALSE && tmp != heap_head) {
    sum += tmp->size;
    tmp = tmp->next_chunk;
    head->next_chunk = tmp;
    ASSERT(head != tmp, "h%p=n%p", head, tmp);
    // dont let heap size get to zero becuase you cant
    // if (!head)
    // heap_size--;
  }
  first_chunk = head;
  first_chunk->size = sum;

#ifdef LOG_COMBINE
  K_LOG("cmb:%d %p", sum, head);
#endif

  return head;
}

// this allocates the created fragment as a freed chunk
void alloc_unused_mm(struct chunk *used_chunk, U32 req_size) {

  struct chunk *head_chunk_t = used_chunk;

  S32 unused_space = used_chunk->size - (req_size + sizeof(struct chunk));

  // align down unused space
  // unused_space &= ~(PAGE_ALIGNMENT - 1);

  // in case of a the chunk is aligned to the min page size
  if (unused_space < 8) {
#define LOG_SPC_ERR
#ifdef LOG_SPC_ERR
    K_LOG("spc:err:%d:%p", unused_space, used_chunk->next_chunk);
#endif
    return;
  }
  // advance the used chunk by 1 sizeof(struct chunk)
  used_chunk++;

  PAGE_SIZE *next_ret = (PAGE_SIZE *)used_chunk + (req_size >> 3);
  struct chunk *freed_mem = (struct chunk *)next_ret;

  freed_mem->next_chunk = head_chunk_t->next_chunk;
  // set used memory next to the unallocated chunk
  (used_chunk - 1)->next_chunk = freed_mem;

  // set the created unused fragment to unused
  freed_mem->flags &= ~(1UL << ALLOCATED_FLG);

  freed_mem->size = unused_space;

#ifdef LOG_UNUSED_ALLOC
  K_LOG("spc:%d:%p", freed_mem->size, freed_mem);
  // K_LOG("spc:next %p", used_chunk);
#endif
}

void *find_freed_chunk(U32 size) {

  struct chunk *cur_p = (struct chunk *)chunk_start;

  while (cur_p != heap_head) {
    // this creates memory fragmentation
    if (((cur_p->flags >> ALLOCATED_FLG) & 1U) == FALSE) {
      combine_chunks(cur_p);
      if (cur_p->size >= size) {
        // ASSERT(cur_p->size < 0xfff,"%d > 0xfff",cur_p->size);
        alloc_unused_mm(cur_p, size);
#ifdef LOG_FND
        K_LOG("fnd:%d %p", cur_p->size, cur_p);
#endif
        return cur_p;
      }
    }
    cur_p = cur_p->next_chunk;
  }

  // if non of the chunks fit the request
  return NULL;
}

void init_heap(void *new_heap) {

  if (new_heap != NULL)
    chunk_start = &new_heap;

  struct chunk *new_chunk = (struct chunk *)chunk_start;
  new_chunk->flags |= (1UL << FIRST_CNK_FLG);
  heap_size++;
}

void *aslloc(S32 size) {

  if (size <= 0)
    return NULL;

  size = (size + PAGE_ALIGNMENT - 1) & ~(PAGE_ALIGNMENT - 1);

  struct chunk *new_chunk = NULL;
  void *freed_chunk = NULL;

  if (heap_size > 0) {

    freed_chunk = (struct chunk *)find_freed_chunk(size);

    if (freed_chunk != NULL) {
      new_chunk = freed_chunk;
    }

    // this expends the heap
    else if (heap_head <= (void *)DYNAMIC_MEM3_BASE ||
             heap_head >= (void *)DYNAMIC_MEM0_BASE) {
      new_chunk = (struct chunk *)heap_head;
      heap_size++;
    } else {
      K_LOG("OOM HIT %d", heap_size);
      return NULL;
    }
  }
  // first allocations
  else {
    new_chunk = (struct chunk *)chunk_start;
    // set FIRST_CHUNK_FLAG to true when makeing the first allocation
    new_chunk->flags |= (1UL << FIRST_CNK_FLG);
    heap_size++;
  }

  // align pages to a sum of 8
 // U32 min_pages = size; //(size + PAGE_ALIGNMENT - 1) & ~(PAGE_ALIGNMENT - 1);

  PAGE_SIZE *next_ret = (PAGE_SIZE *)new_chunk + (size >> 3);

  if (freed_chunk == NULL) {
    new_chunk->next_chunk =
        (struct chunk *)next_ret + 1; // sizeof(struct chunk);
    heap_head = new_chunk->next_chunk;
    new_chunk->next_chunk->next_chunk = NULL;
  }

  // number of actual pages allocated and not the requested size
  new_chunk->size = size;
  new_chunk->flags |= (1UL << ALLOCATED_FLG);

#ifdef LOG_ALLOC
  K_LOG("alc:%d %p", new_chunk->size, new_chunk);
  // K_LOG("alc:nxt %p", new_chunk->next_chunk);
#endif

  total_allocated_mm += new_chunk->size;

  // return data location after meta info
  return new_chunk + 1; // sizeof(struct chunk);
}

void asfree(void *ptr) {

  if (ptr < (void *)DYNAMIC_MEM0_BASE || ptr >= (void *)DYNAMIC_MEM3_BASE) {
    K_LOG("err:fre:%d:%p", heap_size, ptr);
    return;
  }

  // get to the meta chunk info by subtracting the sizeof of the meta struct
  struct chunk *chunk_meta_info =
      (struct chunk *)ptr - 1; // - sizeof(struct chunk);

  // ASSERT(chunk_meta_info->size > 100,"size > 100");
  // set allocated bit to false
  chunk_meta_info->flags &= ~(1UL << ALLOCATED_FLG);

  total_allocated_mm -= chunk_meta_info->size;

#ifdef LOG_FREE
  K_LOG("fre:%d:%p:hep:%d", chunk_meta_info->size, ptr, heap_size);
  // K_LOG("free:all:dyn:mm=%x", total_allocated_mm);
#endif
}

void *reaslloc(void *ptr, U32 size) {

  // pointer to struct meta informetion
  struct chunk *cur_chunk = (struct chunk *)ptr - 1;

  if (cur_chunk->size == size || size <= 0) {
    return ptr;
  }

  if (cur_chunk->size > size) {
    alloc_unused_mm(cur_chunk, size);
    cur_chunk->size = size;
    return cur_chunk + 1;
  }

  void *p = find_freed_chunk(size);

  if (p != NULL) {
    cur_chunk = p;
    return cur_chunk + 1;
  }

  ptr = (U8 *)ptr + 1;
  // TODO implement heap expention
  return (struct chunk *)ptr + 1;
}

U32 alloc_get_size(void *ptr) {
  struct chunk *chunk_meta_info = (struct chunk *)ptr - 1;

  return chunk_meta_info->size;
}

U32 total_alloc_cnk_sum(void) {

  U32 total_allocated_sum = 0;
  struct chunk *cur_p = (struct chunk *)chunk_start;

  while (cur_p != heap_head) {
    if (((cur_p->flags >> ALLOCATED_FLG) & 1U) == TRUE) {
      total_allocated_sum += cur_p->size;
      // K_LOG("0x%p",cur_p);
    }
    cur_p = cur_p->next_chunk;
  }
  return total_allocated_sum;
}

U32 asfree_all(void) {

  U32 total_free = total_alloc_cnk_sum();

  if (total_free == 0)
    return 0;

  U16 cur_free_size = 0;

  struct chunk *cur_p = (struct chunk *)chunk_start;

  while (cur_p != heap_head) {
    if (((cur_p->flags >> ALLOCATED_FLG) & 1U) == TRUE) {
      // plus one becuase asfree() needs the pointer
      // to the actual memory given by aslloc()
      asfree(cur_p + 1);
      cur_free_size += cur_p->size;
    }
    cur_p = cur_p->next_chunk;
  }
  return cur_free_size;
}

void *free_last_alloc(void) {

  if (heap_head == NULL || heap_size == 0)
    return NULL;

  static U16 i;
  static U8 *last_freed;

  if (i == 0)
    last_freed = heap_head;

  else {
    last_freed = last_freed - (((struct chunk *)last_freed)->size);
    K_LOG("last %p", last_freed);
  }

  asfree((struct chunk *)last_freed + 1);

  i++;

  return last_freed;
}
