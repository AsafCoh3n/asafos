/*
 *	Delay functions
 *	See delay.h for details
 *
 *	Make sure this code is compiled with full optimization!!!
 */

#include "type.h"
#include <RTL.h>
#include <LPC24xx.H>

#include "type.h"
#include "delay.h"
#include "glcd.h"

void DelayUs(U16 us) {

  U16 i;
  for (i = 0; i < us / 2; i++) {
    wd();
  } // for
}

void DelayMs(U16 ms) {

  U16 stam;
  U16 i;
  for (i = 0; i <= ms; i++) {
    wd();
    for (stam = 0; stam < 5600; stam++)
      ;
  }
}

void del1sc(void) {
  DelayMs(250);
  DelayMs(250);
  DelayMs(250);
  DelayMs(250);
}
