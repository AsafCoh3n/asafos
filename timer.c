#define PCLK                                                                   \
  12000000 // for 48 mhz PCLK=CCLK/4  - I USE CRYSTAL 10M 10*24/5= 48 MHZ

#include <stdio.h>
#include <RTL.h>
#include <LPC23xx.H> /* LPC23xx definitions               */
#include "Net_Config.h"

#include "type.h"
#include "glcd.h"
#include "kb.h"
#include "timer.h"
#include "irq.h"

U32 cntsec = 0;
U16 cnt1ms = 0;

void Timer0Handler(void) __irq;
volatile U32 timer0_counter = 0;
volatile U32 timer1_counter = 0;

// U16 in_tcp_mode=0;

/*
        Timer/Counter 1 interrupt handler
        executes each 10ms @ 60 MHz CPU Clock

*/

void Timer1Handler(void) __irq {
  T1IR = 1; /* clear interrupt flag */
            //  IENABLE;			/* handles nested interrupt */
  timer1_counter++;

  //  IDISABLE;
  VICVectAddr = 0; /* Acknowledge Interrupt */
}

/*
                Enable timer
                timer number: 0 or 1
*/
void enable_timer(U8 timer_num) {
  if (timer_num == 0) {
    T0TCR = 1;
  } else {
    T1TCR = 1;
  }
  return;
}

/*
                Disable timer
                timer number: 0 or 1
*/
void disable_timer(U8 timer_num) {
  if (timer_num == 0) {
    T0TCR = 0;
  } else {
    T1TCR = 0;
  }
  return;
}

/*
                Reset timer
                timer number: 0 or 1

*/
void reset_timer(U8 timer_num) {
  U32 regVal;

  if (timer_num == 0) {
    regVal = T0TCR;
    regVal |= 0x02;
    T0TCR = regVal;
  } else {
    regVal = T1TCR;
    regVal |= 0x02;
    T1TCR = regVal;
  }
  return;
}

/*
        Initialize timer, set timer interval, reset timer,
                install timer interrupt handler
                timer number and timer interval
                true or false, if the interrupt handler can't be
                installed, return false.

*/
U32 init_timer(U8 timer_num, U32 TmrVal) {

  U64 uul;
  U32 TimerInterval;

  uul = TmrVal;

  uul *= PCLK;
  uul /= 10000000;

  TimerInterval = uul;

  if (timer_num == 0) {

    PCONP |= 0x00000002; // Power Enable Timer0

    timer0_counter = 0;
    T0MR0 = TimerInterval;
    T0MCR = 3; /* Interrupt and Reset on MR0 */
    if (install_irq(TIMER0_INT, (void *)Timer0Handler, HIGHEST_PRIORITY) ==
        FALSE) {
      return (FALSE);
    } else {
      return (TRUE);
    }
  } else if (timer_num == 1) {

    PCONP |= 0x00000004; // Power Enable Timer1

    timer1_counter = 0;
    T1MR0 = TimerInterval;
    T1MCR = 3; /* Interrupt and Reset on MR1 */
    //		if ( install_irq( TIMER1_INT, (void *)Timer1Handler,
    // HIGHEST_PRIORITY ) == FALSE ){
    if (install_irq(TIMER1_INT, (void *)Timer1Handler, LOWEST_PRIORITY) ==
        FALSE) {
      return (FALSE);
    } else {
      return (TRUE);
    }
  }
  return (FALSE);
}

/*
                Timer/Counter 0 interrupt handler
                executes each 10ms @ 60 MHz CPU Clock

*/

void Timer0Handler(void) __irq {

  // static U16 cnt100=0;
  static U16 cnt100;
  static U16 cntXms;

  T0IR = 1; /* clear interrupt flag */
            //	IENABLE;			/* handles nested interrupt */

  cnt1ms++;

  if (cnt100++ > 100) {
    cnt100 = 0;
  }

  if (cntXms++ >= 1000) {
    cntXms = 0;
    cntsec++;
  }

  //	IDISABLE;
  VICVectAddr = 0; /* Acknowledge Interrupt */
}
