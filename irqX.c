
//*   irq.c: Interrupt handler C file for NXP LPC23xx/24xx Family
// Microprocessors

#include <RTL.h>
#include <LPC23xx.H> /* LPC23xx definitions               */
#include "type.h"
#include "irq.h"

/* Initialize the interrupt controller */
/*
** Function name:		init_VIC
**
** Descriptions:		Initialize VIC interrupt controller.
*/

void init_VIC(void) {
  U32 i = 0;
  U32 *vect_addr, *vect_prio;

  /* initialize VIC*/
  VICIntEnClr = 0xffffffff;
  VICVectAddr = 0;
  VICIntSelect = 0;

  /* set all the vector and vector control register to 0 */
  for (i = 0; i < VIC_SIZE; i++) {
    vect_addr = (U32 *)(VIC_BASE_ADDR + VECT_ADDR_INDEX + i * 4);
    vect_prio = (U32 *)(VIC_BASE_ADDR + VECT_PRIO_INDEX + i * 4);
    *vect_addr = 0x0;
    *vect_prio = 0xF;
  }
  return;
}

/******************************************************************************
** Function name:		install_irq
**
** Descriptions:		Install interrupt handler
** parameters:			Interrupt number, interrupt handler address,
**						interrupt priority
** Returned value:		true or false, return false if IntNum is out of
*range
**
******************************************************************************/

U32 install_irq(U32 IntNumber, void *HandlerAddr, U32 Priority) {
  U32 *vect_addr;
  U32 *vect_prio;

  VICIntEnClr = 1 << IntNumber; /* Disable Interrupt */
  if (IntNumber >= VIC_SIZE) {
    return (FALSE);
  } else {
    /* find first un-assigned VIC address for the handler */
    vect_addr = (U32 *)(VIC_BASE_ADDR + VECT_ADDR_INDEX + IntNumber * 4);
    vect_prio = (U32 *)(VIC_BASE_ADDR + VECT_PRIO_INDEX + IntNumber * 4);
    *vect_addr = (U32)HandlerAddr; /* set interrupt vector */
    *vect_prio = Priority;
    VICIntEnable = 1 << IntNumber; /* Enable Interrupt */
    return (TRUE);
  }
}

/******************************************************************************
**                            End Of File
******************************************************************************/
