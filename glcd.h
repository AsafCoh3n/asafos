#include "type.h"

#define LCD_PWR 0x01000000         // P1.24
#define LCD_PWR_DIR IODIR1         // P1.24
#define LCD_PWR_1 IOSET1 = LCD_PWR // P1.24
#define LCD_PWR_0 IOCLR1 = LCD_PWR // P1.24

#define LCD_ENA 0x04000000         // P1.26
#define LCD_ENA_DIR IODIR1         // P1.26
#define LCD_ENA_1 IOSET1 = LCD_ENA // P1.26
#define LCD_ENA_0 IOCLR1 = LCD_ENA // P1.26

#define LCD_RS 0x20000000        // P1.29
#define LCD_RS_DIR IODIR1        // P1.29
#define LCD_RS_1 IOSET1 = LCD_RS // P1.29
#define LCD_RS_0 IOCLR1 = LCD_RS // P1.29

#define LCD_RW 0x08000000        // P1.27
#define LCD_RW_DIR IODIR1        // P1.27
#define LCD_RW_1 IOSET1 = LCD_RW // P1.27
#define LCD_RW_0 IOCLR1 = LCD_RW // P1.27

#define LCD_RES 0x10000000         // P1.28
#define LCD_RES_DIR IODIR1         // P1.28
#define LCD_RES_1 IOSET1 = LCD_RES // P1.28
#define LCD_RES_0 IOCLR1 = LCD_RES // P1.28

#define LCD_CS1 0x00000100          // P2.8
#define LCD_CS1_DIR FIO2DIR         // P2.8
#define LCD_CS1_1 FIO2SET = LCD_CS1 // P2.8
#define LCD_CS1_0 FIO2CLR = LCD_CS1 // P2.8

#define LCD_CS2 0x00000200          // P2.9
#define LCD_CS2_DIR FIO2DIR         // P2.9
#define LCD_CS2_1 FIO2SET = LCD_CS2 // P2.9
#define LCD_CS2_0 FIO2CLR = LCD_CS2 // P2.9

#define LCD_DAT FIO2PIN // P2.0-7
#define LCD_DAT_DIR FIO2DIR
#define LCD_DAT_SET FIO2SET
#define LCD_DAT_CLR FIO2CLR

//#define LCD_DAT_MASK 0xff000000		//P1.24-31
//#define LCD_FIOMASK FIO1MASK		//P1.24-31

#define LEFT 0x01
#define RIGHT 0x02
#define BOTH 0x03
#define NONE 0x00

// internal function prototypes//
// you would not normally call these directly. but you can
// if you like.
#ifdef MEM64K
#define MAX_ANI 8
#else
#define MAX_ANI 4
#endif

void _lcd_enable(void);
void numad_lcd_big(F32 n);
void clr_str(const S8 string[]);
U8 _lcd_status(void);
void _lcd_reset(void);
void lcd_waitbusy(void);
// public function prototypes//
// call these all you want !
void lcd_screenon(U16 on);
void lcd_cls(void);
void lcd_setx(U16 page);
void lcd_sety(U16 y);
void lcd_selectside(U16 sides);
void lcd_write(U16 data);
U8 lcd_read(void);
extern U16 nodisplay;
void lcd_plotpixel(U16 rx, U16 ry);
// void lcd_puts(S8 *string);
// void lcd_putrs(const S8 *string);
void str(const S8 *string, U16 inv);
void str_xy(const S8 *string, U16 x, U16 y, U16 inv);
void lcd_init(void);
void clrlcd(void);
void outlcd(U8 c, U16 inv);
void home(void);
void save_curxy(void);
void lcd_setside(void);
void outlcd_big(U8 c);
void str_xy_big(const S8 *string, U16 x, U16 y);
void str16(const S8 *string);
void str16_xy(const S8 *string, U16 x, U16 y);
void numa_lcd(U8 c, U16 inv_flg);
void numa1(U8 c, U16 inv_flg);
void numai_lcd(U16 c, U16 inv_flg);
void numan_lcd(U16 n, U16 inv_flg);
void bl(void);
void gotol2(void);
void gotol3(void);
void gotol4(void);
void gotol5(void);
void gotol6(void);
void gotol7(void);
void gotol8(void);
void gotoline(U16 lin);
void line_gr(U16 starty, U16 startx, U16 stopy, U16 stopx, U16 on_off);
void draw_sqr(U16 starty, U16 startx, U16 stopy, U16 stopx, U16 on_off);
void plot_xy(U16 x, U16 y, U16 on_off);
void str_hdr(const S8 *string);
void botton(U16 bot, const S8 *st, U16 sts);
void draw_bot(U16 starty, U16 startx, U16 stopy, U16 stopx, U16 on_off);
void draw_mis(U16 starty, U16 startx, U16 stopy, U16 stopx, U16 on_off);
void numaf_lcd(F32 f, U16 big);
void mov_xy(U16 x, U16 y);
void str_center(const S8 *string, U16 lin);
void numanl_lcd(U32 n, U16 inv_flg);
void numan2_lcd(U16 n, U16 inv_flg);
extern void sendPrintScreen(void);
extern void outlcd_xy(U8 a, U16 x, U16 y, U16 inv);
extern U16 curx; // 0-127
extern U16 cury; // 0-7
extern void doron_dis(U16 x, U16 y, const S8 *string, F32 f);
extern void numal_lcd(U32 c, U16 inv_flg);
extern void show_pic(void);
extern void clr_line(U16 lin);

extern void put_lcd_xy(U16 x, U16 y, U8 data);
extern void str_xy_g(S8 *string, U16 x, U16 y, U16 inv, U16 size_flg,
                     U16 dofen_flg);
// extern void circle(U16 x,U16 y,U16 radius,U16 flg);
extern void fan(U16 id, U16 locX, U16 locY, U16 flg_first, U16 flg, U16 type);

extern void putch(U8 byte);
extern void draw_mis_nor(void);
extern void empty_botton(U16 n);
#define SHOW_BLANK -642516.8
extern void read_scr_2_bmp(U8 *pt);
// extern void doron(void);

extern void draw_pic(U8 *pic);
extern void numan_len_lcd(U16 n, U16 len);
extern void draw_arrow(U16 cnt4, U16 locX, U16 locY, U16 type);
extern void wd(void);
extern void botton(U16 bot, const S8 *st, U16 sts);

void klog(char *str);

extern U8 virtual_scr[8][128];

#define DEBUG_MODE 1

#define END_OF_LINE 25
#define MAX_LOG_BUF 32

#if (DEBUG_MODE == 1)
static S8 LOG_BUF[MAX_LOG_BUF];
static U32 log_num;

#include <stdio.h>

#define K_LOG(format, ...)                                                     \
  {                                                                            \
    snprintf(LOG_BUF, MAX_LOG_BUF, "%d>" format, log_num++, ##__VA_ARGS__);    \
    klog(LOG_BUF);                                                             \
  }
	
#define ASSERT(EXPR,MSG,...) \
{	\
	if (!(EXPR)) {	\
		K_LOG(MSG, ##__VA_ARGS__); \
	while(1); \
	}	\
} 
	
#else
#define K_LOG(format, ...)
//	assertion's should be kept defined just without the 
// printing part
//#define ASSERT(EXPR,MSG) 
#endif

// 3d functionality

#define PI 3.1415926536

extern F32 Bar_X, Bar_Z;
extern F32 Gr_X, Gr_Y, Gr_Z, OffsetX, OffsetY;
extern void Make3DPoint(F32 x, F32 y, F32 z);
void SetBarPos(F32 ofs_X, F32 ofs_Z);
extern void RotatePdY(F32 Angle);
extern void RotatePdX(F32 Angle);
extern void DrawCube(U16 ClrX, U16 All);
