/*****************************************************************************
 *   type.h:  Type definition Header file for NXP LPC230x Family
 *   Microprocessors
 ******************************************************************************/
#ifndef __TYPE_H__
#define __TYPE_H__

#ifndef NULL
#define NULL ((void *)0)
#endif

#ifndef FALSE
#define FALSE (0)
#endif

#ifndef TRUE
#define TRUE (1)
#endif

typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned long DWORD;

typedef char S8;
typedef unsigned char U8;
typedef short S16;
typedef unsigned short U16;
typedef int S32;
typedef unsigned int U32;
typedef long long S64;
typedef unsigned long long U64;
typedef unsigned char BIT;
typedef unsigned int BOOL;
typedef float F32;
typedef double F64;

#endif /* __TYPE_H__ */

#define BIT0 (0x01)
#define BIT1 (0x02)
#define BIT2 (0x04)
#define BIT3 (0x08)
#define BIT4 (0x10)
#define BIT5 (0x20)
#define BIT6 (0x40)
#define BIT7 (0x80)
#define BIT8 (0x0100)
#define BIT9 (0x0200)
#define BIT10 (0x0400)
#define BIT11 (0x0800)
#define BIT12 (0x1000)
#define BIT13 (0x2000)
#define BIT14 (0x4000)
#define BIT15 (0x8000)
#define BIT16 (0x010000UL)
#define BIT17 (0x020000UL)
#define BIT18 (0x040000UL)
#define BIT19 (0x080000UL)
#define BIT20 (0x100000UL)
#define BIT21 (0x200000UL)
#define BIT22 (0x400000UL)
#define BIT23 (0x800000UL)
#define BIT24 (0x01000000UL)
#define BIT25 (0x02000000UL)
#define BIT26 (0x04000000UL)
#define BIT27 (0x08000000UL)
#define BIT28 (0x10000000UL)
#define BIT29 (0x20000000UL)
#define BIT30 (0x40000000UL)
#define BIT31 (0x80000000UL)
