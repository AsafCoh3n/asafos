#include "type.h"

#define DYNAMIC_MEM0_BASE 0xA0000000
#define DYNAMIC_MEM1_BASE 0xB0000000
#define DYNAMIC_MEM2_BASE 0xC0000000
#define DYNAMIC_MEM3_BASE 0xD0000000

struct chunk {

  U32 size;
  BYTE flags;
  struct chunk *next_chunk;

} ;

// MAIN API
void init_heap(void *new_heap);
void *aslloc(U32 size);
void asfree(void *ptr);
void *reaslloc(void *ptr, U32 size);
//

U32 alloc_get_size(void *ptr);
U32 total_alloc_cnk_sum(void);
U32 asfree_all(void);
void *free_last_alloc(void);


//extern void *head_heap;

