// TCP commends define

const char *cur_cmd[] = {
	"echo",
	"read",
	"write",
	"ping",
	"printscr",
	"readsec",
	"alloc",
	"free",
	"freeall",
	"help",
	"freel",
	"clear",
};

#define ECHO 0
#define READ_BYTE 1
#define WRITE_BYTE 2
#define PING 3
#define PRINT_SCR 4
#define READ_SECTOR 5
#define ASLLOC_REM 6
#define ASFREE_REM 7
#define FREE_ALL 8
#define HELP 9
#define FREE_LAST 10 
#define CLEAR_LCD 11


