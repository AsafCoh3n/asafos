
#include <stdio.h>
#include <RTL.h>
#include <LPC23xx.H> /* LPC23xx definitions               */

#include "type.h"
#include "glcd.h"
#include "kb.h"

void init_kb(void) {
  KB_DIR1 &= ~KB_BIT1;
  KB_DIR2 &= ~KB_BIT2;
  KB_DIR3 &= ~KB_BIT3;
  KB_DIR4 &= ~KB_BIT4;
  KB_DIR5 &= ~KB_BIT5;
  KB_DIR6 &= ~KB_BIT6;
}

U16 get_kb_bit(void) {

  U16 c = 0;

  if (KB_IN1 == 0)
    c = 1;
  if (KB_IN2 == 0)
    c = 2;
  if (KB_IN3 == 0)
    c = 3;
  if (KB_IN4 == 0)
    c = 4;
  if (KB_IN5 == 0)
    c = 5;
  if (KB_IN6 == 0)
    c = 6;

  return c;
}
