/*
its basicly the grphics api for the lcd and the logic

*/
#define NO_LCD 0 //

//#include "flgs.h"
//#include "defines.h"
//#include <RTL.h>
//#include "type.h"
#include <LPC23xx.H> /* LPC23xx definitions               */
#include "type.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>       // cos(),sin()
#include "Net_Config.h" //for eth setting option
//#include <absacc.h>
//#include <math.h>

#include "Glcd.h"
#include "delay.h"
#include "Main.h" //additon to add timer_pool
#include "kb.h"
#include "nv_ram.h" //non volatile ram
#include "timer.h"

//#include "Main.h"
//#include "timeprog.h"
//#include "program.h"
//#include "scr_util.h"
//#include "rs485.h"
//#include "fcode.h"
//#include "alarm.h"
//#include "tcp.h"

U16 curx;  // 0-127
U16 cury;  // 0-7
U16 curyG; // 0-63
U16 save_curx;
U16 save_cury;
U16 save_curyG;
U16 cur_side;

U16 nodisplay = 0;
void outlcd_g(U8 c, U16 inv, U16 sml_flg);
U16 RS;

U16 virtual_lcd_mode = 0; // write to scr to buf

U8 virtual_scr[8][128]; // 8 lines of 128 bytes | represents the screen

#define PI 3.1415926536

void lcd_setyG(unsigned char y);

U8 GET_LCD_DATA(void);
void WRITE_LCD_DATA(U8 dat);

#define BASE_CG_HEB 150
#define BASE_CG_RUS 177

const S8 font8x8[] = {
#include "CG6X8.H"
};

const S16 font12x16[] = {
#include "CG12X16.H"
};

const S8 font5x5[] = {
#include "CG5X5.H"
};

void _lcd_enable(void) {
  lcd_setside();
  LCD_ENA_1; // ENABLE=1;
  LCD_ENA_0; // ENABLE=0;
}

U8 _lcd_status(void) { // returns the lcd status & maintains the TRIS state of
                       // the lcd data port
  U32 dat;
  U8 a;

  if (virtual_lcd_mode != 0)
    return 0;

  LCD_RS_0; // DI=0;
  RS = 0;
  LCD_RW_1; // RW=1; // command/read

  LCD_DAT_DIR &= 0xffffff00; // make it input

  lcd_setside();
  LCD_ENA_1; // ENABLE=1;

  dat = LCD_DAT;

  LCD_ENA_0; // ENABLE=0;

  dat &= 0x000000ff;
  a = dat;
  LCD_DAT_DIR |= 0x000000ff; // make it output

  return a;
}

void _lcd_reset(void) { // reset the lcd module

  lcd_selectside(BOTH);
  // after Vdd Fs 4.5v.
  // from experimentation, this is bullshit. this seems to
  // work though.
  LCD_RES_0;   // LCD_RESET=0;
  DelayMs(10); // actually .5 ms
  LCD_RES_1;   // LCD_RESET=1;
  DelayMs(50); // actually .5 ms
  // check status, and wait if necessary
  while (_lcd_status() & 0x10) {
    DelayUs(250); // .5 ms
    DelayUs(250); // .5 ms
  }
}

void lcd_screenon(U16 on) { // turn the display on or off

  LCD_RW_0; // RW=0;
  LCD_RS_0; // DI=0;
  RS = 0;

  WRITE_LCD_DATA(0x3e | (on & 1)); // main screen turn on!

  curx = 64;
  _lcd_enable();
  curx = 0;
  _lcd_enable();
}
void clrlcd(void) {
  U16 x, y;

  for (y = 0; y < 8; y++) { // set the page (y)
    lcd_setx(0);
    lcd_waitbusy();
    lcd_sety(y); // set the y address to 0
    lcd_waitbusy();
    // setup for data
    WRITE_LCD_DATA(0);
    LCD_RW_0; // RW=0;
    LCD_RS_1; // DI=1;
    RS = 1;
    // clear the row
    for (x = 0; x < 64; x++) {
      _lcd_enable();
      DelayUs(20);
    }
  }

  memset(virtual_scr, 0, sizeof(virtual_scr)); // clear virtual memory

  for (y = 0; y < 8; y++) { // set the page (y)
    lcd_setx(64);
    lcd_waitbusy();
    lcd_sety(y);
    lcd_waitbusy();
    // setup for data
    WRITE_LCD_DATA(0);
    LCD_RW_0; // RW=0;
    LCD_RS_1; // DI=1;
    RS = 1;
    // clear the row
    for (x = 64; x < 128; x++) {
      _lcd_enable();
      DelayUs(20);
    }
  }
  home();
}

void lcd_sety(U16 page) {

  cury = page;

  if (virtual_lcd_mode != 0)
    return;

  lcd_waitbusy();
  LCD_RS_0; // DI=0;
  RS = 0;
  LCD_RW_0; // RW=0;
  WRITE_LCD_DATA(0xb8 | (page));
  _lcd_enable();
}

void lcd_setx(U16 x) {

  if (x > 127) {
    x = 0;
  }

  curx = x;

  if (virtual_lcd_mode != 0)
    return;

  lcd_waitbusy();
  LCD_RS_0; // DI=0;
  RS = 0;
  LCD_RW_0; // RW=0;
  WRITE_LCD_DATA(0x40 | (x & 0x3f));
  _lcd_enable();
}

void lcd_waitbusy(void) {
  U16 try
    = 0;

  U8 sts;

#if NO_LCD
  return;
#endif

  if (virtual_lcd_mode != 0)
    return;

  while (1) {
    sts = _lcd_status();
    sts &= 0x80;
    if (sts == 0)
      return;
    //!!DelayUs(250); // .5 ms
    //!!DelayUs(250); // .5 ms
    if (try ++ > 2000) {
      lcd_init();
    }
  }
}

void lcd_write(U16 data) {

  if (RS != 0) {
    if (curx > 127) {
      curx = 0;
    }
    virtual_scr[cury][curx] = data; // 8 lines of 128 bytes
  }
  if (virtual_lcd_mode != 0) {
    curx++;
    return;
  }

  lcd_setside();
  lcd_waitbusy();
  LCD_RS_1; // DI=1;
  RS = 1;
  LCD_RW_0; // RW=0;
  WRITE_LCD_DATA(data);
  _lcd_enable();
  curx++;
  if (curx == 64) { // we just pass page
    lcd_setside();
    lcd_setx(curx);
    lcd_sety(cury);
  }
}

void lcd_setside(void) {
  if (cur_side == 0) {
    if (curx > 63) {
      lcd_selectside(RIGHT);
    }
  } else {
    if (curx < 64) {
      lcd_selectside(LEFT);
    }
  }
}

void lcd_selectside(U16 sides) { // set a CS pin low to enable it
  if (sides & LEFT) {
    cur_side = 0;
    LCD_CS1_0; // CS1 = 0;
  } else {
    LCD_CS1_1; // CS1 = 1;
  }

  if (sides & RIGHT) {
    LCD_CS2_0; // CS2 = 0;
    cur_side = 1;
  } else {
    LCD_CS2_1; // CS2 = 1;
  }
}

U8 lcd_read(void) {
  U8 _data;
  LCD_RW_1; // RW=1;
  LCD_RS_1; // DI=1;
  RS = 1;

  lcd_setside();
  _data = GET_LCD_DATA();
  return _data;
}

void outlcd(U8 c, U16 inv) {

  U8 i;
  U8 a;
  U16 base;

  if (inv & 2) { // flg for no NewLine
    inv &= 0xfd;
    if (c == '_')
      c = '-';
  }

  if ((c == '_') || (c == '\n')) {

    curx = 0;
    cury++;
    cury &= 7;
    lcd_setx(curx);
    lcd_sety(cury);
    return;
  }

  //	lang=LANGUAGE();
  //	switch (lang){
  //		case HEBREW:
  //			if (c>=224){
  //				c=c-(224-BASE_CG_HEB);
  //			}
  //			break;

  //	}

  if (c >= 224) {
    c = c - (224 - BASE_CG_HEB);
  }

  save_curxy();
  base = c - ' ';
  base *= 6;
  for (i = 0; i < 6; i++) {
    a = font8x8[base + i];
    if (inv != 0)
      a = 255 - a;
    lcd_write(a);
  }

  lcd_setx(save_curx + 6);
  lcd_sety(save_cury);
}
void outlcd_big(U8 c) {

  U8 wd = 12;
  U8 i;
  U16 base;

  save_curxy();
  base = c - '0';
  switch (c) {
  case '.':
    base = 10; // char 10 = '.'
    wd = 6;
    break;
  case ' ':
    base = 11; // char 11 = ' '
    break;
  case '-':
    base = 12; // char 12 = '-'
    break;
  case ':':
    base = 13; // char 13 = ':'
    break;
  }

  base *= 12;

  if (curx < 128) {
    for (i = 0; i < wd; i++) {
      lcd_write((U8)(font12x16[base + i] & 255));
    }

    lcd_setx(save_curx);
    lcd_sety(save_cury + 1);

    for (i = 0; i < wd; i++) {
      lcd_write((U8)(font12x16[base + i] >> 8));
    }
  }

  lcd_setx(save_curx + wd);
  lcd_sety(save_cury);
}

void str(const S8 *string, U16 inv) {
  S8 i = 0;
  while (string[i] != 0) {
    outlcd(string[i++], inv);
  }
}

void str_xy(const S8 *string, U16 x, U16 y, U16 inv) {

  S8 i = 0;

  lcd_setx(x);
  lcd_sety(y);

  while (string[i] != 0)
    outlcd(string[i++], inv);
}

void outlcd_xy(U8 a, U16 x, U16 y, U16 inv) {
  lcd_setx(x);
  lcd_sety(y);
  outlcd(a, inv);
}

void str_xy_big(const S8 *string, U16 x, U16 y) {

  S8 i = 0;

  lcd_setx(x);
  lcd_sety(y);

  if (string[0] == 1)
    i = 1; // ignore first char if ==1

  while (string[i] != 0)
    outlcd_big(string[i++]);
}

void lcd_init(void) {

  // U16 i;

  LCD_PWR_DIR |= LCD_PWR; // set to out
  LCD_PWR_0;              // LCD ON     RE7
  DelayMs(100);

  LCD_DAT_DIR |= 0x000000ff; // all outputs

  LCD_ENA_DIR |= LCD_ENA; // set to out

  LCD_RW_DIR |= LCD_RW; // set to out
  LCD_RS_DIR |= LCD_RS; // set to out

  LCD_RES_DIR |= LCD_RES; // set to out
  LCD_CS1_DIR |= LCD_CS1; // set to out
  LCD_CS2_DIR |= LCD_CS2; // set to out

  _lcd_reset();
  lcd_screenon(1);
  DelayMs(10);
}

void save_curxy(void) {
  save_curx = curx;
  save_cury = cury;
  save_curyG = curyG;
}

void numa_lcd(U8 c, U16 inv_flg) {
  U16 n;

  c = (U8)c & 255;

  n = c / 16;
  numa1(n, inv_flg);
  n = c & 15;
  numa1(n, inv_flg);
}

void numa1(U8 c, U16 inv_flg) {
  if (c > 9)
    c += 7;
  outlcd(c + 48, inv_flg);
}

void numal_lcd(U32 c, U16 inv_flg) {
  U32 n;
  n = c;
  c = c >> 16;
  c = c & 0xffff;
  n = n & 0xffff;
  numai_lcd(c, inv_flg);
  numai_lcd(n, inv_flg);
}

void numai_lcd(U16 c, U16 inv_flg) {
  U16 n;
  n = c;
  c = c >> 8;
  numa_lcd((c & 0x00ff), inv_flg);
  numa_lcd((n & 0x00ff), inv_flg);
}

void numan_lcd(U16 n, U16 inv_flg) {
  S8 st[20];
  sprintf(st, "%3d", n);
  str(st, inv_flg);
}

void numan2_lcd(U16 n, U16 inv_flg) {
  S8 st[20];
  sprintf(st, "%2d", n);
  str(st, inv_flg);
}

void numanl_lcd(U32 n, U16 inv_flg) {
  S8 st[20];
  sprintf(st, "%69lu", n);
  str(st, inv_flg);
}

void bl(void) { outlcd(' ', 0); }

void gotol2(void) { gotoline(1); }
void gotol3(void) { gotoline(2); }
void gotol4(void) { gotoline(3); }

void gotol5(void) { gotoline(4); }

void gotol6(void) { gotoline(5); }
void gotol7(void) { gotoline(6); }
void gotol8(void) { gotoline(7); }

void gotoline(U16 lin) { // 0..7
  lcd_setx(0);           // set the y address to 0
  lcd_sety(lin);
}

void home(void) {
  gotoline(0);
  RS = 1;
}

/*
void line_gr(U16 starty,U16 startx,U16 stopy,U16 stopx, U16 on_off){

        S16 difx,dify;
        S16 x,y,step;

        difx=stopx-startx;
        dify=stopy-starty;

        step=0;
        if (abs(difx)>abs(dify)){
                x=startx;
nxt_x:
                y=(U16)(starty+(F32)step*(F32)dify/(F32)(abs(difx+1)));

                plot_xy(x,y,on_off);
                if (x==stopx) goto after_line;
                if (stopx>startx){
                        x++;
                }else{
                        x--;
                }
                step++;
                goto nxt_x;

        }else{
                y=starty;
nxt_y:
                x=(U16)(startx+(F32)step*(F32)difx/(F32)(abs(dify+1)));

                plot_xy(x,y,on_off);
                if (y==stopy) goto after_line;
                if (stopy>starty){
                        y++;
                }else{
                        y--;
                }
                step++;
                goto nxt_y;
        }

after_line:;

}

*/

void line_gr(U16 starty, U16 startx, U16 stopy, U16 stopx, U16 on_off) {

  int difx, dify;
  float x, y, step;
  float int_x, int_y;

  U16 dir;

  if (stopx >= startx) {
    difx = stopx - startx;
  } else {
    difx = startx - stopx;
  }
  difx++;
  if (stopy >= starty) {
    dify = stopy - starty;
  } else {
    dify = starty - stopy;
  }
  dify++;

  x = startx;
  y = starty;

  if (abs(difx) > abs(dify)) {
    // x is laregr than y so make the x step by 1
    step = (dify - 1.0) / (difx - 1.0);
    step = fabs(step);
    if (stopy < starty) { // neg in y step
      step = -step;
    }

    int_x = x;
    dir = 0; // positive
    if (stopx < startx)
      dir = 1; // neg in x step

  nxt_x:
    int_y = (U16)(y + 0.5);
    //		tft_plot_xy(int_x,int_y,colr);
    plot_xy(int_x, int_y, on_off);

    if (int_x == stopx)
      goto after_line;

    if (dir == 0) {
      int_x++;
    } else {
      int_x--;
    }

    y += step;
    goto nxt_x;

  } else {
    // y is laregr than x so make the y step by 1
    step = (difx - 1.0) / (dify - 1.0);
    step = fabs(step);
    if (stopx < startx) { // neg in x step
      step = -step;
    }
    int_y = y;

    dir = 0; // positive
    if (stopy < starty)
      dir = 1; // neg in y step

  nxt_y:
    int_x = (U16)(x + 0.5);
    // tft_plot_xy(int_x,int_y,colr);
    plot_xy(int_x, int_y, on_off);

    if (int_y == stopy)
      goto after_line;
    if (dir == 0) {
      int_y++;
    } else {
      int_y--;
    }

    x += step;
    goto nxt_y;
  }

after_line:;
}

void draw_sqr(U16 starty, U16 startx, U16 stopy, U16 stopx, U16 on_off) {
  line_gr(starty, startx, stopy, startx, on_off);
  line_gr(starty, startx, starty, stopx, on_off);
  line_gr(stopy, startx, stopy, stopx, on_off);
  line_gr(starty, stopx, stopy, stopx, on_off);
}

void draw_mis(U16 starty, U16 startx, U16 stopy, U16 stopx, U16 on_off) {
  line_gr(starty, startx, stopy, startx, on_off);
  line_gr(starty, startx, starty, startx + 10, on_off);
  line_gr(stopy, startx, stopy, startx + 10, on_off);

  line_gr(starty, stopx, stopy, stopx, on_off);

  line_gr(starty, stopx - 10, starty, stopx, on_off);
  line_gr(stopy, stopx - 10, stopy, stopx, on_off);
}

void draw_bot(U16 starty, U16 startx, U16 stopy, U16 stopx, U16 on_off) {

  if (stopx > 127)
    stopx = 127;

  line_gr(starty, startx, stopy, startx, on_off);
  line_gr(starty, startx, starty, stopx, on_off);
  line_gr(starty, stopx, stopy, stopx, on_off);
}

const U8 bit_tbl[] = {1, 2, 4, 8, 16, 32, 64, 128};

void plot_xy(U16 x, U16 y, U16 on_off) {

  // x = 0-127;
  // y = 0-63;

  U8 data;
  U8 v;

  x &= 0x7f;
  y &= 0x3f;

  lcd_setx(x);
  lcd_sety(y >> 3);

  // lcd_waitbusy();    //asaf addtion

  data = lcd_read();
  // lcd_waitbusy();     //asaf addtion

  data = lcd_read();

  v = bit_tbl[y & 7];
  if (on_off == 0) {
    data = data & (255 - v);
  } else {
    data = data | v;
  }

  lcd_setx(x);

  lcd_write(data);
}

void str_hdr(const S8 *string) {

  U8 i, a, j;

  home();
  i = strlen(string);

  if (i > 21) { // 21*6=126 bits - max display
    str(string, 1);
  } else {
    a = 6 * i;

    i = 128 - a;
    a = i / 2;
    i = i - a;

    for (j = 0; j < a; j++) {
      lcd_waitbusy();
      lcd_write(0);
    }
    str(string, 0);
    for (j = 0; j < i; j++) {
      lcd_waitbusy();
      lcd_write(0);
    }
  }
}

void str_center(const S8 *string, U16 lin) {

  // add 100 to lin - make it inverse

  U8 i, a;
  U8 inv = 0;

  if (lin >= 100) {
    lin -= 100;
    inv = 1;
  }

  mov_xy(0, lin);

  i = strlen(string);

  if (i > 21) { // 21*6=126 bits - max display
    str(string, 0);
  } else {
    a = 6 * i;
    i = 128 - a;
    a = i / 2;

    mov_xy(a, lin);
    str(string, inv);
  }
}

void botton(U16 bot, const S8 *st, U16 sts) {
  // bot = 0..3 (left to right)
  // st  = string inside
  // sts = 0=off , 1 =push

  U8 i, x, k;

  i = strlen(st);
  k = 0;

  if (strcmp(st, "KEY_UP") == 0) {
    k = 130;
  }
  if (strcmp(st, "KEY_DOWN") == 0) {
    k = 132;
  }
  if (strcmp(st, "KEY_LEFT") == 0) {
    k = 134;
  }
  if (strcmp(st, "KEY_RIGHT") == 0) {
    k = 136;
  }

  if (k != 0) {
    x = 10 + (bot << 5);
    lcd_setx(x);
    lcd_sety(7);
    outlcd(k, 0);
    outlcd(k + 1, 0);
  } else {
    x = 1 + (bot << 5) + ((30 - 6 * i) >> 1);
    str_xy(st, x, 7, 0);
  }

  draw_bot(54, 0 + (32 * bot), 63, 32 + (32 * bot), 1);
}

void mov_xy(U16 x, U16 y) {
  lcd_setx(x);
  lcd_sety(y);
}

// void numaf_lcd(F32 f,U16 big){
void numaf_lcd(F32 d, U16 big) {
  S8 st[20];
  S8 s;

  F32 f;
  f = (F32)d;

  if (big == 2) { // print without format
    sprintf(st, "%8f", f);
    big = 0;
    goto do_numf;
  }

  if (f < 0) {
    if (f <= (-1000)) {
      st[0] = '-';
      st[1] = '9';
      st[2] = '9';
      st[3] = '9';
      st[4] = '9';
      st[5] = '.';
      st[6] = 0;
      s = 6;
    } else {
      if (f <= (-100)) {
        s = sprintf(st, "%5.1f", f);
      } else {
        if (f <= (-10)) {
          s = sprintf(st, "%5.2f", f);
        } else {
          if (f <= (-1)) {
            s = sprintf(st, "%5.3f", f);
          } else {
            s = sprintf(st, "%5.4f", f);
          }
        }
      }
    }
  } else {
    if (f >= 100000) {
      st[0] = '9';
      st[1] = '9';
      st[2] = '9';
      st[3] = '9';
      st[4] = '9';
      st[5] = '.';
      st[6] = 0;
      s = 6;
    } else {
      if (f >= 10000) {
        s = sprintf(st, "%5lu", (U32)f);
        st[5] = '.';
        st[6] = 0;
        s = 6;
      } else {
        if (f >= 100) {
          s = sprintf(st, "%6.1f", f);
        } else {
          if (f >= 10) {
            s = sprintf(st, "%6.2f", f);
          } else {
            s = sprintf(st, "%6.3f", f);
          }
        }
      }
    }
  }
  if (s > 6) {
    st[6] = 0;
  }

do_numf:

  if (big == 0) {
    str_xy(st, curx, cury, 0);
  } else {
    if (big == 3) {
      if (st[0] == ' ')
        st[0] = 1;
    }
    str_xy_big(st, curx, cury);
  }
}

void numad_lcd_big(F32 d) {

  S8 st[20];
  F32 f;
  f = (F32)d;

  if (d == SHOW_BLANK)
    return;

  if (f < 0) {
    if (f <= -(100000)) {
      sprintf(st, "-%7lu", (U32)-d);
    } else {
      d = d * 10.0;
      sprintf(st, "-%6lu", (U32)-d);
      st[8] = 0;
      st[7] = st[6];
      st[6] = '.';
      curx += 6; // because of the point
    }
  } else {
    if (d >= 100000) {
      sprintf(st, "%8lu", (U32)d);
    } else {
      d = d * 100.0;
      sprintf(st, "%7lu", (U32)d);
      st[8] = 0;
      st[7] = st[6];
      st[6] = st[5];
      st[5] = '.';
      curx += 6; // because of the point
    }
  }

  str_xy_big(st, curx, cury);
}

void clr_line(U16 lin) {
  U16 i;
  mov_xy(1, lin);
  for (i = 0; i < 21; i++) {
    bl();
  }
}

U8 GET_LCD_DATA(void) {

  //	if (virtual_lcd_mode!=0){
  return virtual_scr[cury][curx]; // 8 lines of 128 bytes
  //}
  /*
U8 a;
//U32 dat;

LCD_DAT_CLR=0x000000ff;
lcd_waitbusy();

a = LCD_DAT;
return a;
*/
}

void WRITE_LCD_DATA(U8 dat) {

  U32 a;

  if (RS != 0) {
    if (curx > 127) {
      curx = 0;
    }
    virtual_scr[cury][curx] = dat; // 8 lines of 128 bytes
  }
  if (virtual_lcd_mode != 0)
    return;

  a = dat;
  LCD_DAT_CLR = 0x000000ff;

  LCD_DAT_SET = a;
}

void read_scr_2_bmp(U8 *pt) {
  U16 x, y, i;
  U8 data, bt;
  U8 v;

  // x = 0-127;
  // y = 0-63;

  //	const U8 bit_tbl[]={1,2,4,8,16,32,64,128};

  //	U8 buf_line[128];

  for (y = 0; y < 8; y++) {
    //		for (x=0;x<128;x++){ //read 128 bytes

    /*
                            lcd_setx(x);
                            lcd_sety(7-y);  //bmp start from botton to top
                            lcd_waitbusy();

                            data = lcd_read();
                            lcd_waitbusy();
                            data = lcd_read();
    */
    //			buf_line[x]=virtual_scr[y][x]; //8 lines of 128 bytes
    //		}

    for (i = 0; i < 8; i++) { // tipul 8 lines
      bt = bit_tbl[7 - i];
      data = 0;
      for (x = 0; x < 128; x++) {
        data = data << 1;

        v = virtual_scr[7 - y][x];

        if ((v & bt) == 0)
          data |= 1;

        if ((x & 7) == 7) { // every 8 bits - write the byte
          *pt = data;
          pt++;
        }
      }
    }
  }
}

void draw_pic(U8 *pic) {

  U16 x, y, c;
  U8 *p;

  p = pic;

  for (y = 0; y < 8; y++) { // set the page (y)
    lcd_setx(0);
    lcd_waitbusy();
    lcd_sety(y); // set the y address to 0
    lcd_waitbusy();
    LCD_RW_0; // RW=0;
    LCD_RS_1; // DI=1;
    RS = 1;
    for (x = 0; x < 64; x++) {
      c = *p;
      WRITE_LCD_DATA(c);
      _lcd_enable();
      //!!DelayUs(100);
      p++;
    }
    p += 64;
  }

  p = pic + 64;

  for (y = 0; y < 8; y++) { // set the page (y)
    lcd_setx(64);
    lcd_waitbusy();
    lcd_sety(y);
    lcd_waitbusy();
    // setup for data
    LCD_RW_0; // RW=0;
    LCD_RS_1; // DI=1;
    RS = 1;
    // clear the row
    for (x = 64; x < 128; x++) {
      c = *p;
      WRITE_LCD_DATA(c);
      _lcd_enable();
      //!!DelayUs(100);
      p++;
    }
    p += 64;
  }
}

void put_lcd_xy(U16 x, U16 y, U8 data) {

  // put 1 byte top to bottom (least on top)

  // x = 0-127; in jump of 8
  // y = 0-63;

  lcd_setx(x);
  lcd_sety(y >> 3);

  lcd_waitbusy();
  lcd_write(data);
}

void str_xy_g(S8 *string, U16 x, U16 y, U16 inv, U16 size_flg, U16 dofen_flg) {

  S8 i = 0;

  x &= 127;
  y &= 63;

  lcd_setx(x);
  lcd_setyG(y);

  while (string[i] != 0)
    outlcd_g(string[i++], inv, size_flg);
}

void putch(U8 byte) { outlcd(byte, 0); }

void draw_mis_nor(void) { draw_mis(0, 0, 50, 127, 1); }

const U8 mask_tbl12a[] = {0, 1, 3, 7, 15, 31, 63, 127};
const U8 mask_tbl7[] = {0x80, 1, 3, 7, 15, 31, 63, 127};
const U8 mask_tbl5[] = {0xe0, 0xc1, 0x83, 7, 15, 31, 63, 127};

void outlcd_g(U8 c, U16 inv, U16 size_flg) {

  U8 i;
  U8 a;
  U16 base;
  U16 aa;
  U8 ofs;
  U8 data;
  U8 wd;

  U8 flg_big7 = 0;

  if (size_flg > 2) {
    size_flg = 0;
  }

  if ((c >= 'a') && (c <= 'z')) {
    c -= 32; // Upper Case
  }

  if ((c >= '?') && (c <= '?')) {
    c = c - (224 - 97);
  }

  save_curxy();

  ofs = curyG & 7;

  base = c - ' ';

  switch (size_flg) {
  case 0:
    wd = 5;
    base *= wd;
    break;
  case 1:
    wd = 6;
    base *= wd;
    break;
  case 2:
    wd = 12;
    base = c - '0';
    switch (c) {
    case '.':
      base = 10; // S8 10 = '.'
      break;
    case ' ':
      base = 11; // S8 11 = ' '
      break;
    case '-':
      base = 12; // S8 12 = '-'
      break;
    case ':':
      base = 13; // char 13 = ':'
      break;
    }

    flg_big7 = 0;
    if ((c >= 'A') && (c <= 'Z')) {
      base = 17 + c - 'A';
      base *= wd; //*12
    } else {
      if (base > 16) { // its font that we don't have - so we use 6x8 instead
        flg_big7 = 1;
        base = c - ' ';
        base *= 6;
      } else {
        base *= wd; //*12
      }
    }
    break;
  }

  for (i = 0; i < wd; i++) {

    lcd_setx(curx);
    lcd_sety(cury);
    lcd_waitbusy();

    data = lcd_read();
    lcd_waitbusy();
    data = lcd_read(); // get the exist byte

    switch (size_flg) {
    case 0:
      a = font5x5[base + i];
      a = a & 31; // only 5 U16
      if (inv != 0)
        a = 31 - a;
      break;
    case 1:
      a = font8x8[base + i];
      a = a & 127; // only 7 U16
      if (inv != 0)
        a = 127 - a;
      break;
    case 2:
      a = font12x16[base + i] & 255;
      if (inv != 0)
        a = 255 - a;
      if (flg_big7 != 0) {
        a = font8x8[base + i];
        if (inv != 0)
          a = 15 - a;
        a = a << 4;
        if (i >= 6)
          a = 0;
      }
      break;
    }
    //		if (inv!=0) a=255-a;
    a = a << ofs;

    switch (size_flg) {
    case 0:
      data &= mask_tbl5[ofs];
      break;
    case 1:
      data &= mask_tbl7[ofs];
      break;
    case 2:
      data &= mask_tbl12a[ofs];
      break;
    }
    data |= a;

    lcd_setx(curx);
    lcd_write(data);
  }

  switch (size_flg) {
  case 0:
    if (ofs < 4)
      goto ex_lg;
    break;
  case 1:
    if (ofs < 2)
      goto ex_lg;
    break;
  }

  // do second line

  cury++;
  curx -= wd;

  for (i = 0; i < wd; i++) {

    lcd_setx(curx);
    lcd_sety(cury);
    lcd_waitbusy();

    data = lcd_read();
    lcd_waitbusy();
    data = lcd_read(); // get the exist byte

    switch (size_flg) {
    case 0:
      a = font5x5[base + i];
      aa = a & 31; // only 5 U16
      if (inv != 0)
        aa = 31 - aa;
      data &= (255 - mask_tbl7[ofs - (7 - wd)]);
      break;
    case 1:
      a = font8x8[base + i];
      aa = a & 127; // only 7 U16
      if (inv != 0)
        aa = 127 - aa;
      data &= (255 - mask_tbl7[ofs - (7 - wd)]);
      break;
    case 2:
      aa = font12x16[base + i];
      if (inv != 0)
        aa = 255 - aa;
      data = 0;
      if (flg_big7 != 0) {
        aa = font8x8[base + i];
        if (inv != 0)
          aa = 15 - aa;
        aa = aa << 4;
        if (i >= 6)
          aa = 0;
      }
      break;
    }

    aa = aa >> (8 - ofs);

    a = aa & 255;

    data |= a;

    lcd_setx(curx);
    lcd_write(data);
  }

  switch (size_flg) {
  case 0:
  case 1:
    goto ex_lg;
  case 2:
    if (ofs == 0)
      goto ex_lg;
    break;
  }

  // do third line

  cury++;
  curx -= wd;

  for (i = 0; i < wd; i++) {

    lcd_setx(curx);
    lcd_sety(cury);
    lcd_waitbusy();

    data = lcd_read();
    lcd_waitbusy();
    data = lcd_read(); // get the exist byte

    aa = font12x16[base + i];
    if (inv != 0)
      aa = 255 - aa;
    if (flg_big7 != 0) {
      aa = font8x8[base + i];
      if (inv != 0)
        aa = 15 - aa;
      aa = aa << 4;
      if (i >= 6)
        aa = 0;
    }

    aa = aa >> (16 - ofs);

    a = aa & 255;

    data &= (255 - mask_tbl12a[ofs]);

    data |= a;

    lcd_setx(curx);
    lcd_write(data);
  }

ex_lg:
  lcd_setx(save_curx + wd);
  lcd_sety(save_cury);
  curyG = save_curyG;
}

void lcd_setyG(unsigned char y) {
  curyG = y;
  cury = y >> 3;

  if (virtual_lcd_mode != 0)
    return;

  lcd_waitbusy();
  LCD_RS_0; // DI=0;
  RS = 0;
  LCD_RW_0; // RW=0;
  WRITE_LCD_DATA(0xb8 | (cury));
  _lcd_enable();
}

void str16(const S8 *string) { str(string, 0); }

void numaf16(F32 f) { numaf_lcd(f, 0); }

void outlcd16(U8 a) { outlcd(a, 0); }

void numan16(U32 n) { numan_lcd(n, 0); }

void bl16(void) { bl(); }

F32 Bar_X, Bar_Z;

F32 Gr_X, Gr_Y, Gr_Z, OffsetX, OffsetY;

struct P_REC {
  F32 x;
  F32 y;
  F32 z;
};

struct T_REC {
  U16 p1;
  U16 p2;
};

struct TAG_REC {
  U16 x;
  U16 y;
};

struct P_REC Pd[8]; // Data Point
struct P_REC Px[8];
void G_drawLine(U16 startx, U16 starty, U16 stopx, U16 stopy, U16 colr);

void Make3DPoint(F32 x, F32 y, F32 z) {

  Pd[0].x = 0;
  Pd[0].y = -y;
  Pd[0].z = -z;
  Pd[1].x = x;
  Pd[1].y = -y;
  Pd[1].z = -z;
  Pd[2].x = x;
  Pd[2].y = 0;
  Pd[2].z = -z;
  Pd[3].x = 0;
  Pd[3].y = 0;
  Pd[3].z = -z;
  Pd[4].x = 0;
  Pd[4].y = -y;
  Pd[4].z = 0;
  Pd[5].x = x;
  Pd[5].y = -y;
  Pd[5].z = 0;
  Pd[6].x = x;
  Pd[6].y = 0;
  Pd[6].z = 0;
  Pd[7].x = 0;
  Pd[7].y = 0;
  Pd[7].z = 0;
}

void tft_setpixel_xy(U16 x, U16 y, U16 colrx) { plot_xy(x, y, colrx); }

void x3d_Line(U16 x1, U16 y1, U16 x2, U16 y2, U16 colr) {
  G_drawLine(x1, y1, x2, y2, colr);
}

void RotateCoordinatesX(F32 Angle) {
  U16 i;
  F32 NewY, NewZ;
  for (i = 0; i < 8; i++) {
    NewY = Px[i].y * cos(Angle) - Px[i].z * sin(Angle);
    NewZ = Px[i].y * sin(Angle) + Px[i].z * cos(Angle);
    Px[i].y = NewY;
    Px[i].z = NewZ;
  }
}

void DrawFillSqrData(U16 p1, U16 p2, U16 p3, U16 p4, U16 colrx) {

  S16 ix1, ix2, iy1, iy2;
  S16 ix3, iy3, ix4, iy4;

  ix1 = Pd[p1].x;
  iy1 = Pd[p1].y;
  ix2 = Pd[p2].x;
  iy2 = Pd[p2].y;
  ix3 = Pd[p3].x;
  iy3 = Pd[p3].y;
  ix4 = Pd[p4].x;
  iy4 = Pd[p4].y;

  // colrx=get_color(COLR_G2D_BORDER);

  x3d_Line(ix1 + OffsetX, iy1 + OffsetY, ix4 + OffsetX, iy4 + OffsetY,
           colrx); // LEFT
  x3d_Line(ix2 + OffsetX, iy2 + OffsetY, ix3 + OffsetX, iy3 + OffsetY,
           colrx); // RIGHT
  x3d_Line(ix1 + OffsetX, iy1 + OffsetY, ix2 + OffsetX, iy2 + OffsetY,
           colrx); // top
  x3d_Line(ix4 + OffsetX, iy4 + OffsetY, ix3 + OffsetX, iy3 + OffsetY,
           colrx); // bottom
}

void DrawCube(U16 ClrX, U16 All) {

  DrawFillSqrData(0, 1, 2, 3, ClrX);

  DrawFillSqrData(0, 4, 5, 1, ClrX);

  DrawFillSqrData(1, 5, 6, 2, ClrX);

  if (All != 0) {
    DrawFillSqrData(3, 7, 6, 2, ClrX);
    DrawFillSqrData(0, 4, 7, 3, ClrX);
    DrawFillSqrData(4, 5, 6, 7, ClrX);
  }
}

void SetBarPos(F32 ofs_X, F32 ofs_Z) {

  U16 i;

  for (i = 0; i < 8; i++) {
    Pd[i].x = Pd[i].x + ofs_X;
    Pd[i].z = Pd[i].z - ofs_Z;
  }
}

void RotatePdY(F32 Angle) {
  U16 i;
  F32 NewX, NewZ;
  for (i = 0; i < 8; i++) {
    NewX = Pd[i].z * sin(Angle) + Pd[i].x * cos(Angle);
    NewZ = Pd[i].z * cos(Angle) - Pd[i].x * sin(Angle);
    Pd[i].x = NewX;
    Pd[i].z = NewZ;
  }
}

void G_drawLine(U16 startx, U16 starty, U16 stopx, U16 stopy, U16 colr) {

  F32 difx, dify;
  F32 x, y, step;
  F32 int_x, int_y;

  U16 dir;

  if (stopx >= startx) {
    difx = stopx - startx;
  } else {
    difx = startx - stopx;
  }
  difx++;
  if (stopy >= starty) {
    dify = stopy - starty;
  } else {
    dify = starty - stopy;
  }
  dify++;

  x = startx;
  y = starty;

  if (abs(difx) > abs(dify)) {
    // x is laregr than y so make the x step by 1
    step = (dify - 1.0) / (difx - 1.0);
    step = fabs(step);
    if (stopy < starty) { // neg in y step
      step = -step;
    }

    int_x = x;
    dir = 0; // positive
    if (stopx < startx)
      dir = 1; // neg in x step

  nxt_x:
    int_y = (U16)(y + 0.5);
    tft_setpixel_xy(int_x, int_y, colr);

    if (int_x == stopx)
      goto after_line;

    if (dir == 0) {
      int_x++;
    } else {
      int_x--;
    }

    y += step;
    goto nxt_x;

  } else {
    // y is laregr than x so make the y step by 1
    step = (difx - 1.0) / (dify - 1.0);
    step = fabs(step);
    if (stopx < startx) { // neg in x step
      step = -step;
    }
    int_y = y;

    dir = 0; // positive
    if (stopy < starty)
      dir = 1; // neg in y step

  nxt_y:
    int_x = (U16)(x + 0.5);
    tft_setpixel_xy(int_x, int_y, colr);

    if (int_y == stopy)
      goto after_line;
    if (dir == 0) {
      int_y++;
    } else {
      int_y--;
    }

    x += step;
    goto nxt_y;
  }

after_line:;
}

void RotatePdX(F32 Angle) {

  U16 i;
  F32 NewY, NewZ;
  for (i = 0; i < 8; i++) {
    NewY = Pd[i].y * cos(Angle) - Pd[i].z * sin(Angle);
    NewZ = Pd[i].y * sin(Angle) + Pd[i].z * cos(Angle);
    Pd[i].y = NewY;
    Pd[i].z = NewZ;
  }
}

BOOL read_scr_xy(U16 x, U16 y) {

  // return BitVal(virtual_scr[x/64][y],);
  return 0;
}

void bla(void) { outlcd('-', 0); } // debug

void clr_block(U16 lin, U16 block) {

#define MAX_BLOCK 22

  if (block > MAX_BLOCK) {
    block = MAX_BLOCK;
  }

  mov_xy(1, lin);

  for (U16 i = 1; i < block; i++) {
    bl();
  }
}

void klog(S8 *str) {

#define MAX_LAST 9

  static U16 print_num, last_len[MAX_LAST];

  if (print_num == MAX_LAST)
    print_num = 0;

  U16 cur_len = strlen(str);

  if (cur_len > END_OF_LINE) {
    str[END_OF_LINE - 1] = '>';
    str[END_OF_LINE] = '\0';
    cur_len = MAX_BLOCK; // END_OF_LINE - 4;
  }

  if (cur_len < last_len[print_num])
    clr_block(print_num, last_len[print_num]);

  last_len[print_num] = cur_len;

  str_xy_g(str, 1, 8 * print_num++, 0, 0, 0);
  // DelayMs(100);
}
