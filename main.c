#include <stdio.h>
#include <RTL.h>
#include <LPC23xx.H> /* LPC23xx definitions */

#include "type.h"
#include "glcd.h"
#include "kb.h"
#include "timer.h"
#include "exsdram.h"

#define MCLK 48000000          /* Master Clock 48 MHz         */
#define TCLK 10                /* Timer Clock rate 10/s       */
#define TCNT (MCLK / TCLK / 4) /* Timer Counts                */

//#define I2C_AA     0x00000004
//#define I2C_SI     0x00000008
//#define I2C_STO    0x00000010
//#define I2C_STA    0x00000020
//#define I2C_I2EN   0x00000040

U16 Pass100Ms = 0; // 100ms
U32 timer = 0;
BOOL click = 0;
BOOL tick;

/*--------------------------- init ------------------------------------------*/
static void init() {

  PCONP |= (1 << 7); /* Enable clock for I2C0        */

  /* Configure I2C */
  PCONP |= 0x00000080; /* Enable clock for I2C0        */
  PINSEL1 |= 0x01400000;
  //  I20CONCLR =  I2C_AA | I2C_SI | I2C_STA | I2C_I2EN;
  //  I20SCLL   =  0x80;                         /* Set I2C Clock speed */
  //  I20SCLH   =  0x80;
  //  I20CONSET =  I2C_I2EN;
  //  I20CONSET =  I2C_STO;

  /* Timer 1 as interval timer, reload to 100ms. */
  T1TCR = 1;
  T1MCR = 3;
  T1MR0 = TCNT - 1;

  /* Configure UART1 for 115200 baud. */
  PINSEL1 |= 0x30003FFF; /* Enable UART1 pins            */
  U1LCR = 0x83;          /* 8 bits, no Parity, 1 Stop bit*/
  U1DLL = 3;             /* for 12MHz PCLK Clock         */
  U1FDR = 0x67;          /* Fractional Divider           */
  U1LCR = 0x03;          /* DLAB = 0                     */
}

/*--------------------------- LED_out ---------------------------------------*/

void LED_out(U32 val) {
  //  U32 v;

  //  v = (val & 0x01) | ((val << 1) & 0x04) | ((val << 2) & 0x10) | ((val << 3)
  //  & 0x40); I20CONCLR =  I2C_AA | I2C_SI | I2C_STA | I2C_STO; I20CONSET =
  //  I2C_STA; while (!(I20CONSET & I2C_SI));        /* Wait for START */ I20DAT
  //  =  0xC0; I20CONCLR =  I2C_SI | I2C_STA; while (!(I20CONSET & I2C_SI)); /*
  //  Wait for ADDRESS send             */ I20DAT    =  0x18; I20CONCLR =
  //  I2C_SI; while (!(I20CONSET & I2C_SI));        /* Wait for DATA send */
  //  I20DAT    =  v;
  //  I20CONCLR =  I2C_SI;
  //  while (!(I20CONSET & I2C_SI));        /* Wait for DATA send */ I20CONSET =
  //  I2C_STO; I20CONCLR =  I2C_SI; while (I20CONSET & I2C_STO);          /*
  //  Wait for STOP                     */
}

/*--------------------------- timer_poll ------------------------------------*/
// deleted staic to call from other files
void timer_poll(void) {
  /* System tick timer running in poll mode */

  U16 Cnt10 = 0;
  U32 CntSec = 0; // 1sec
  if (T1IR & 1) {
    T1IR = 1;
    /* Timer tick every 100 ms */
    timer_tick();
    tick = __TRUE;

    Pass100Ms++; // for inside main runtime

    if (++Cnt10 > 9) {
      Cnt10 = 0;
      CntSec++;
    }
  }
}

/*--------------------------- sendchar --------------------------------------*/

int sendchar(int ch) {
  /* Debug output to serial port. */

  if (ch == '\n') {
    while (!(U1LSR & 0x20))
      ;
    U1THR = '\r'; /* output CR                    */
  }
  while (!(U1LSR & 0x20))
    ;
  return (U1THR = ch);
}

/*--------------------------- getkey ----------------------------------------*/

int getkey(void) {
  /* A dummy function for 'retarget.c' */
  return (0);
}

/*--------------------------- blink_led -------------------------------------*/

void wd(void) {
  WDFEED = 0xAA;
  WDFEED = 0x55;
}

void init_leds(void) {
  LED1_DIR |= LED1;
  LED2_DIR |= LED2;
  LED3_DIR |= LED3;
  LED4_DIR |= LED4;
}

void set_led(U16 n, U16 sts) {
  switch (n) {
  case 0: // led 1
    if (sts == 0) {
      LED1_0;
    } else {
      LED1_1;
    }
    break;
  case 1: // led 2
    if (sts == 0) {
      LED2_0;
    } else {
      LED2_1;
    }
    break;
  case 2: // led 3
    if (sts == 0) {
      LED3_0;
    } else {
      LED3_1;
    }
    break;
  case 3: // led 4
    if (sts == 0) {
      LED4_0;
    } else {
      LED4_1;
    }
    break;
  }
}

#define TCP_SUPPORT

/*---------------------------------------------------------------------------*/

#include "dynmem.h"
#include <stdlib.h>
#include <string.h>

int main(void) {

#ifdef TCP_SUPPORT

#include "eth.h"

#endif

  SDRAMInit();

  lcd_init(); // initialization to lcd

  clrlcd(); // clears lcd
  init();

#ifdef TCP_SUPPORT
  init_TcpNet();
#endif

  init_timer(0, 10000); // 1 ms - after tcp !!!
  enable_timer(0);
  init_kb();
  init_leds();

#ifdef TCP_SUPPORT
  /* initialize the TCP */
  tcp_soc[0] = tcp_get_socket(TCP_TYPE_SERVER, 0, 30, tcp_recive_callback);

  if (tcp_soc[0 != 0]) {
    /* start listening on TCP port 1337 hax0r */
    tcp_listen(tcp_soc[0], PORT);
  }

#endif

  // when given NULL it defualts to the defualt heap start location
  // init_heap(NULL);

  // AsafOS allocator implementation memory fragmentation test

#define SZ 1

#define PAGE_ALIGNMENT 32
  for (U16 i = 0; i < SZ; i++) {
    U16 rand_int = (rand() % SZ) + 1;
    ASSERT(rand_int != 0, "rand(%d)==0", i);
    U8 *buf1 = aslloc(rand_int);
    U8 *buf2 = aslloc(rand_int/2);
		memset(buf1,0xff,rand_int);
		memset(buf2,0xff,rand_int/2);
    asfree(buf1);
		U8 *buf3 = aslloc(rand_int/3);
		memset(buf3,0xff,rand_int/3);
		asfree(buf2);
		asfree(buf3);
		
 }
	
 
  U16 key, Led_Sts, Led_Num = 0;

  while (1) {
		main_TcpNet();

#ifdef TCP_SUPPORT
#endif
    // send_data();

    if (cnt1ms > 100) {
      cnt1ms = 0;
      timer_tick();


      Led_Num++;
      Led_Num &= 7; // Run 0-7
      if (Led_Num > 3) {
        Led_Sts = 1;
      } else {
        Led_Sts = 0;
      }
      set_led(Led_Num & 3, Led_Sts);
      key = get_kb_bit();
    }
    switch (key) {

    case 1:
      K_LOG("key:%d", key);
      break;
    case 2:
      clrlcd();
      break;
    case 3:
      K_LOG("key:%d", key);
      break;
    case 4:
      break;
    case 5:
      clrlcd();
      break;
    case 6:
//      DelayMs(200); // actually .5 ms
      break;
    }
    key = 0;
#ifdef TCP_SUPPORT

#endif
  }
}
