U16 tcp_send_callback(U8 soc, U8 event, U8 *ptr, U16 par);
U16 tcp_recive_callback(U8 soc, U8 event, U8 *ptr, U16 par);
// void send_data(void);

extern U8 tcp_soc[2];
extern U8 soc_state;
#define PORT 1337
#define MAX_NLOG_BUF 256

extern U8 output_buf[MAX_NLOG_BUF];

#define N_LOG(format, ...)                                                     \
  {                                                                            \
    sprintf((S8 *)output_buf,format,##__VA_ARGS__);                          \
		while(send_data() == FALSE);																							\
  }

