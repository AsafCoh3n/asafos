
void init_kb(void);
U16 get_kb_bit(void);
void enter_kb_bit(U16 kb);
extern U16 kb_overwrite;

#define KB_BIT1 BIT26 // P3.26
#define KB_BIT2 BIT25 // P3.25
#define KB_BIT3 BIT24 // P3.24
#define KB_BIT4 BIT25 // P2.25
#define KB_BIT5 BIT26 // P2.26
#define KB_BIT6 BIT23 // P3.23

#define KB_DIR1 FIO3DIR
#define KB_DIR2 FIO3DIR
#define KB_DIR3 FIO3DIR
#define KB_DIR4 FIO2DIR
#define KB_DIR5 FIO2DIR
#define KB_DIR6 FIO3DIR

#define KB_IN1 (FIO3PIN & KB_BIT1)
#define KB_IN2 (FIO3PIN & KB_BIT2)
#define KB_IN3 (FIO3PIN & KB_BIT3)
#define KB_IN4 (FIO2PIN & KB_BIT4)
#define KB_IN5 (FIO2PIN & KB_BIT5)
#define KB_IN6 (FIO3PIN & KB_BIT6)

#define LED1 BIT18 // P1.18 (ALARM)
#define LED1_DIR IODIR1
#define LED1_1 IOSET1 = LED1
#define LED1_0 IOCLR1 = LED1

#define LED2 BIT19 // P1.19 (COMM)
#define LED2_DIR IODIR1
#define LED2_1 IOSET1 = LED2
#define LED2_0 IOCLR1 = LED2

#define LED3 BIT20 // P1.20 (PROG)
#define LED3_DIR IODIR1
#define LED3_1 IOSET1 = LED3
#define LED3_0 IOCLR1 = LED3

#define LED4 BIT21 // P1.21 (RUN)
#define LED4_DIR IODIR1
#define LED4_1 IOSET1 = LED4
#define LED4_0 IOCLR1 = LED4
