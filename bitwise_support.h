// bit feild functions
#define BIT_VAL(data, y) ((data >> y) & 1)      /** Return Data.Y value   **/
#define SET_BIT(data, y) data |= (1 << y)       /** Set Data.Y   to 1    **/
#define CLEAR_BIT(data, y) data &= ~(1 << y)    /** Clear Data.Y to 0    **/
#define TOGGLE_BIT(data, y) (data ^= BitVal(y)) /** Togle Data.Y  value  **/
#define TOGGLE(data) (data = ~data)             /** Togle Data value  **/
